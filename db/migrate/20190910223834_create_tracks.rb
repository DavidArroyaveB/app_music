# frozen_string_literal: true

class CreateTracks < ActiveRecord::Migration[6.0]
  def change
    create_table :tracks do |t|
      t.string :title
      t.string :album
      t.string :year
      t.string :artist

      t.timestamps
    end
  end
end
