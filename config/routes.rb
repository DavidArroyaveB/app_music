# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :usuarios
  get 'spotify/search'
  resources :tracks
  resources :playlists
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
  root to: 'tracks#index'
end
