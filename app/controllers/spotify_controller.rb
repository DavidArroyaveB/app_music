# frozen_string_literal: true

class SpotifyController < ApplicationController
  def search
    title = params[:title]
    @results = RSpotify::Artist.search(title) if title
  end
end
