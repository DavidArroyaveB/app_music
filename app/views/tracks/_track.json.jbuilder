# frozen_string_literal: true

json.extract! track, :id, :title, :album, :year, :artist, :created_at, :updated_at
json.url track_url(track, format: :json)
