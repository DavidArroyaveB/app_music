# frozen_string_literal: true

class Track < ApplicationRecord
  validates_presence_of :title, :artist
end
