# frozen_string_literal: true

class Playlist < ActiveRecord::Base
  validates_presence_of :name, :number_votes
end
